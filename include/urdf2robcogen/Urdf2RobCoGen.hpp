/*! \file  Urdf2RobCogen.hpp
 *  \brief Class for generating Kindsl files from URDF
 *  \authors depardo
 *  \date Aug 29, 2017
 */

#ifndef _URDF2ROBCOGEN_HPP_
#define _URDF2ROBCOGEN_HPP_

#include <ros/ros.h>
#include <ros/package.h>
#include <urdf/model.h>
#include <kindr/Core>
#include <memory>
#include <fstream>
#include <sstream>
#include <boost/shared_ptr.hpp>
#include <Eigen/Dense>


/**
 * @brief This class allows users to generate the RobCoGen description files (.kindsl & .dtdsl)
 * from the Unifyed Robot Description Format (URDF) convention.
 */
class Urdf2RobCoGen {

public:

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  typedef std::map<std::string,kindr::RotationQuaternion<double>,std::less<std::string>,
      Eigen::aligned_allocator<std::pair<const std::string,kindr::RotationQuaternion<double>>>> QuaternionMap_t;

  /*!
   * @brief Create a new URDF to Robcogen instance opening an urdf file
   * @param[in] path_to_urdf_file the path to the urdf file
   */
  Urdf2RobCoGen(const std::string & path_to_urdf_file, const bool & debug_flag = false);


  /*!
   * @brief Create a new URDF to RobCoGen instance loading the urdf model from the parameter server
   * @param parameter_server a bool for verifying the use of the parameter server
   * @param param_name a string with the name of the parameter
   */
  Urdf2RobCoGen(const bool & parameter_server , const std::string & param_name, const bool & debug_flag = false);

  ~Urdf2RobCoGen(){}

  /*!
   * @brief User has to set the name of the robot
   * @param robot_name
   */
  void setRobotName(const std::string & robot_name) { robot_name_ = robot_name;}

  /*!
   * @brief Use the links in the URDF to set the relevant links
   */
  void setLinkNamesFromUrdf();

  /*!
   * @brief Use a list of JOINTS names to set the relevant links
   * @param joint_names a std::vector with the names of the joints to be considered
   */
  void setLinkNamesFromJointNames(const std::vector<std::string> & joint_names);

  /*!
   * @brief Set the relevant links from a list of names
   * @param link_names a std::vector with the names of the relevant links
   */
  void setLinkNames(const std::vector<std::string> & link_names);

  /*!
   * @brief starts the generation process. Two files (.kindsl & .dtdsl are saved to disk at the end of this process)
   * @return a bool representing the success of the file generation
   */
  bool generateFiles();

  /*!
   * @bief Additional frames to be added to the links
   * @param frame an std::vector with the names of the frames (and end effectors)
   * @param parents as std::vector with the names corresponding parent LINKS
   * @param translation an std::vector with the position of the frame w.r.t. parent frame
   * @param rotation an std::vector with the orientation of the frame w.r.t. parent frame
   */
  void setAdditionalFrames(const std::vector<std::string> & frame ,
                           const std::vector<std::string> & parents,
                           const std::vector<urdf::Vector3> & translation,
                           const std::vector<urdf::Vector3> & rotation);

  /*!
   * @brief set the names of the FRAMES that are consider end effectors
   * @param ee_names an std::vector with the names of the frames to be considered end effectors
   */
  void setEENames(const std::vector<std::string> & ee_names);

  /*!
   *
   */
  void setDebugMode(const bool & flag) { debug_mode_ = flag; }

private:
  urdf::Model urdf_model_;
  std::string robot_name_;
  std::vector<std::string> robot_link_names_;
  std::vector<std::string> robot_joint_names_;
  std::vector<std::string> ee_names_;
  std::vector<std::string> orphan_frames_;
  std::map<std::string,urdf::Joint> frames_;
  std::map<std::string,urdf::Joint> orphan_joints_;
  std::vector<std::string> virt_links_;
  std::vector<std::string> virt_joints_;

  std::vector<std::vector<std::string>> branches_;
  std::string root_link_name_;
  bool frames_names_ready_ = false;
  bool link_names_ready_ = false;
  bool branhces_ready_ = false;
  bool ee_names_ready_ = false;

private:

  std::string print_inertia_params(const urdf::Link & link) const;
  std::string print_children(const urdf::Link & link);
  std::string print_translation(const urdf::Joint & joint);
  std::string print_rotation(const urdf::Joint & joint);
  urdf::Vector3 rotation_vector(const urdf::Rotation & rot_quaternion);
  std::string print_frame(const urdf::Link & link);
  std::string print_vector(const urdf::Vector3 & vector) const;
  std::string list_frames() const;
  std::string list_transforms();
  std::string list_jacobians();

  bool generateKindsl();
  bool generateDtDsl();
  void compute_branches();
  void fix_URDF_frames();
  void showLinkJointNames();
  void setRootLinkName();

  bool debug_mode_ = false;
};

Urdf2RobCoGen::Urdf2RobCoGen(const std::string & path_to_urdf_file,
                             const bool & debug_flag):robot_name_("myRobot") {

  debug_mode_ = debug_flag;
  //read the urdf file
  if(!urdf_model_.initFile(path_to_urdf_file)) {
      ROS_ERROR("Invalid URDF File");
      exit(EXIT_FAILURE);
  }

  setRootLinkName();

  std::cout << "reading urdf from a file..." << std::endl;
  std::cout << "the root of the urdf model is : " << root_link_name_ << std::endl;
  if(debug_mode_) {
      std::cout << "press <any key> to continue..." << std::endl;
      std::getchar();
  }
}

Urdf2RobCoGen::Urdf2RobCoGen(const bool & parameter_server, const std::string & param_name,
                             const bool & debug_flag) {

  debug_mode_ = debug_flag;
  if(!parameter_server) {
      ROS_ERROR("Invalid Parameter Server");
      std::exit(EXIT_FAILURE);
  }

  if(!urdf_model_.initParam(param_name)) {
      ROS_ERROR("Invalid URDF Parameter");
      exit(EXIT_FAILURE);
  }

  setRootLinkName();

  std::cout << "reading urdf from a the parameter server..." << std::endl;
  std::cout << "the root of the urdf model is : " << root_link_name_ << std::endl;
  if(debug_mode_) {
      std::cout << "press <any key> to continue..." << std::endl;
      std::getchar();
  }
}

void Urdf2RobCoGen::setRootLinkName() {

  root_link_name_ = urdf_model_.getRoot()->name;

  if(!(urdf_model_.getRoot()->inertial)) {
      std::cout << std::endl;
      std::cout << "The root link of the robot does not have inertia properties!" << std::endl;
      std::cout << "please select among the child links for setting these properties :" << std::endl;
      size_t op = 0;
      for(auto link : urdf_model_.getRoot()->child_links) {
          std::cout << op << ". " << link->name << std::endl;
          op++;
      }
      std::cout << op << ". None (set properties manually in the kindsl file later)" << std::endl;
      int user_input;
      std::cin >> user_input;
      if(user_input < 0 || user_input > op) {
          std::cout << "invalid input!" << std::endl;
          std::exit(EXIT_FAILURE);
      } else  {
          if(!(user_input == op)){
            // the virtual link
            urdf::Link vlink = *urdf_model_.getRoot()->child_links[user_input];
            // copy the inertia properties to the root
            urdf_model_.root_link_->inertial = boost::shared_ptr<urdf::Inertial>(new urdf::Inertial);
            urdf_model_.root_link_->inertial = urdf_model_.getRoot()->child_links[user_input]->inertial;
            // virtual links and joints
            virt_links_.push_back(vlink.name);
            virt_joints_.push_back(vlink.parent_joint->name);
            for(auto j : vlink.child_joints) {
                virt_joints_.push_back(j->name);
            }
          }
      }
  }
}

void Urdf2RobCoGen::setLinkNames(const std::vector<std::string> & link_names){

  if(link_names_ready_) {
      std::cout << "link names already exist and are not going to change!" << std::endl;
      std::cout << "press <any key> to continue..." << std::endl;
      std::getchar();
  }
    return;

  robot_link_names_ = link_names;
  link_names_ready_ = true;
  robot_joint_names_.clear();

  // get the joint names from a list of link_names
  // ToDo: This might create a situation where there are more than 1 root_links!
  //     : create a std::map with the tree of links and check there is only 1 root_link
  for(auto l_name : link_names) {
      urdf::Link one_link = *urdf_model_.getLink(l_name);
      robot_joint_names_.push_back(one_link.parent_joint->name);
  }

  if(debug_mode_) {
      std::cout << "User is setting Link Names." << std::endl;
      std::cout << "Joints names are therefore extracted from the URDF." << std::endl;
      showLinkJointNames();
  }
}

void Urdf2RobCoGen::setLinkNamesFromUrdf() {

  if(link_names_ready_) {
    std::cout << "link names already exist and are not going to change!" << std::endl;
    std::cout << "press <any key> to continue..." << std::endl;
    std::getchar();
    return;
  }

  for(auto link_map:urdf_model_.links_) {

      // only if it is not a virtual link
      auto test = find(virt_links_.begin(),virt_links_.end(),link_map.first);
      if(test == virt_links_.end() && link_map.first != root_link_name_) {

        // check that the link is actually a link and not a frame
        if(!link_map.second->inertial) {
            std::cout << "The urdf link : " << link_map.first << " does not have inertial properties!" << std::endl;

            // not a link, but maybe a frame or a 'mounting frame'
            // if no children, then definitely a frame
            if(link_map.second->child_joints.empty()) {
                std::cout << "and it does not have any children frames, therefore it is a FRAME" << std::endl;
                orphan_frames_.push_back(link_map.first);
                orphan_joints_[link_map.second->parent_joint->name] = *link_map.second->parent_joint;
            }
            else {
                  std:: cout << "and it has children joints, therefore it is a 'mounting frame'" << std::endl;
                  for(auto joint : link_map.second->child_joints) {
                      std::cout << "child of " << link_map.second->name << " : " << joint->name << std::endl;
                      orphan_joints_[joint->name] = *joint;
                  }
                  // what are the joints that connect with this link?
                  orphan_joints_[link_map.second->parent_joint->name] = *link_map.second->parent_joint;
            }
            std::getchar();
        } else {
            robot_link_names_.push_back(link_map.first);
        }
      }
  }

  for(auto joint_map:urdf_model_.joints_) {

    // only if it is not a virtual joint
    auto n_virt = find(virt_joints_.begin(),virt_joints_.end(),joint_map.first);
    if(n_virt == virt_joints_.end()) {

      // only if it is not a orphan joint
      auto test = orphan_joints_.find(joint_map.first);
      if(test == orphan_joints_.end()) {
        robot_joint_names_.push_back(joint_map.first);
      }
    }
  }

  // fix the orphan_frames : Add frames to the parent using parent info.
  for( auto orphan : orphan_frames_){

      std::string frame_name = orphan + "_o";
      std::string frame_parent_link  = urdf_model_.links_[orphan]->parent_joint->parent_link_name;
      std::string frame_parent_joint = urdf_model_.links_[orphan]->parent_joint->name;

      urdf::Joint single_frame;
      single_frame.name = frame_name;
      single_frame.parent_link_name=frame_parent_link;
      single_frame.parent_to_joint_origin_transform.position.clear();
      single_frame.parent_to_joint_origin_transform.position.x = urdf_model_.joints_[frame_parent_joint]->parent_to_joint_origin_transform.position.x;
      single_frame.parent_to_joint_origin_transform.position.y = urdf_model_.joints_[frame_parent_joint]->parent_to_joint_origin_transform.position.y;
      single_frame.parent_to_joint_origin_transform.position.z = urdf_model_.joints_[frame_parent_joint]->parent_to_joint_origin_transform.position.z;

      double x,y,z;
      urdf_model_.joints_[frame_parent_joint]->parent_to_joint_origin_transform.rotation.getRPY(x,y,z);
      single_frame.parent_to_joint_origin_transform.rotation.setFromRPY(x,y,z);
      frames_[frame_name] = single_frame;
  }

  link_names_ready_ = true;

  if(debug_mode_) {
      std::cout << "Reading Link Names fom URDF" << std::endl;
      std::cout << "Joints names are extracted from the URDF as well" << std::endl;
      showLinkJointNames();
  }
}

void Urdf2RobCoGen::setLinkNamesFromJointNames(const std::vector<std::string> & joint_names) {

  if(link_names_ready_) {
    std::cout << "link names already exist and are not going to change from this list of joints!" << std::endl;
    std::cout << "press <any key> to continue..." << std::endl;
    std::getchar();
    return;
  }
    robot_joint_names_ = joint_names;

    // get the name of the links in the active joints
   for(auto it = joint_names.begin(); it != joint_names.end() ; ++it) {

       std::string parent = urdf_model_.joints_[*it]->parent_link_name;
       std::string child  = urdf_model_.joints_[*it]->child_link_name;

       if(parent!=root_link_name_) {
         auto test_p = find(robot_link_names_.begin(), robot_link_names_.end(),parent);
         if(test_p == robot_link_names_.end())
           robot_link_names_.push_back(parent);
       }
       //remove this as this is impossible!
       if(child!=root_link_name_) {
         auto test_c = find(robot_link_names_.begin(), robot_link_names_.end(),child);
         if(test_c == robot_link_names_.end())
           robot_link_names_.push_back(child);
       }
   }
   link_names_ready_ = true;

   if(debug_mode_) {
       std::cout << "User is setting Joints names." << std::endl;
       std::cout << "Links are therefore extracted from the URDF as well" << std::endl;
       showLinkJointNames();
   }
}

void Urdf2RobCoGen::setAdditionalFrames(const std::vector<std::string> & frames ,
                                        const std::vector<std::string> & parents,
                                        const std::vector<urdf::Vector3> & translation,
                                        const std::vector<urdf::Vector3> & rotation){
  if(!link_names_ready_) {
      std::cout << "please set the links names before using this method" << std::endl;
      std::exit(EXIT_FAILURE);
  }

  // frames, parents, translations and rotations should has same size
  int nFrames = frames.size();
  if(nFrames != parents.size() || nFrames != translation.size() || nFrames != rotation.size()) {
      std::cout << "frames, parents, translation and rotation vectors should have the same size" << std::endl;
      std::exit(EXIT_FAILURE);
  }

  // all parents must exist already in the chain
  for(auto p:parents) {
      auto test_p = std::find(robot_link_names_.begin(), robot_link_names_.end(),p);
      if(test_p == robot_link_names_.end() && p != root_link_name_){
          std::cout << "All parents must correspond to an existing Link," << std::endl;
          std::cout << "Parent: " << p << ", does not exist!" << std::endl;
          std::exit(EXIT_FAILURE);
      }
  }

  // save the frames, its parents and pose
  for(int k = 0 ; k < frames.size() ; ++k) {
      urdf::Joint single_frame;
      single_frame.name = frames[k];
      single_frame.parent_link_name=parents[k];
      single_frame.parent_to_joint_origin_transform.position=translation[k];
      single_frame.parent_to_joint_origin_transform.rotation.setFromRPY(rotation[k].x,rotation[k].y,rotation[k].z);
      frames_[frames[k]] = single_frame;
  }

  if(debug_mode_) {
      std::cout << "User is setting additional frames." << std::endl;
      for(auto fname : frames_)
          std::cout << fname.first << std::endl;
      std::cout << "press <any key> to continue..." << std::endl;
      std::getchar();
  }

  // adding a frame at the CoM of each link to represent the inertia_params
  // as the URDF uses the CoM as frame for the inertial parameters
  // This should no affect the link-joint-frame transformations
  for(auto link : robot_link_names_) {

      if(urdf_model_.links_[link]->inertial) {

        std::string com_name =  link + "_com";
        if(debug_mode_)
          std::cout << "adding COM : " << com_name << std::endl;
        urdf::Joint single_frame;
        single_frame.name = com_name;
        single_frame.parent_link_name = link;

        single_frame.parent_to_joint_origin_transform = urdf_model_.links_[link]->inertial->origin;
        frames_[com_name] = single_frame;

      } else {
          std::cout << "The link : " << link << "has missing inertial information" << std::endl;
      }

      // now the inertial origin position must be set to zero according to the ROBCOGEN documentation
      //urdf_model_.links_[link]->inertial->origin.position = urdf::Vector3(0,0,0);
  }
  if(debug_mode_) {
      std::cout << "press <any key> to continue..." << std::endl;
      std::getchar();
  }

  frames_names_ready_ = true;

  if(debug_mode_) {
      std::cout << "The final list of frames, including Links' COM: " << std::endl;
      for(auto fname : frames_)
          std::cout << fname.first << " <-- " << fname.second.parent_link_name << std::endl;
      std::cout << "press <any key> to continue..." << std::endl;
      std::getchar();
  }

}

void Urdf2RobCoGen::setEENames(const std::vector<std::string> & ee_names) {

  if(!frames_names_ready_) {
      std::cout << "EE Names must be set after setting the additional frames!" << std::endl;
      std::exit(EXIT_FAILURE);
  }

  //check that all the names are in the list of additional frames
  for(auto e : ee_names) {
      bool name_ok = false;
      for(auto f : frames_) {
          if(f.second.name == e) {
              name_ok = true;
              break;
          }
      }
      if(!name_ok) {
          std::cout << "The end effector " << e << " is not in the list of additional frames" << std::endl;
          std::exit(EXIT_FAILURE);
      }
  }
  ee_names_ = ee_names;
  ee_names_ready_ = true;

  if(debug_mode_) {
      std::cout << "User is setting the EE frames names." << std::endl;
      for(auto name:ee_names)
          std::cout << name << std::endl;
      std::cout << "press <any key> to continue..." << std::endl;
      std::getchar();
  }
}

void Urdf2RobCoGen::compute_branches() {

  size_t nBranches = ee_names_.size();

  for(auto e_e : ee_names_) {
      std::vector<std::string> single_branch;
      single_branch.push_back(e_e);

      urdf::Joint j = frames_[e_e];
      urdf::Link linkb = *urdf_model_.links_[j.parent_link_name];
      while(linkb.name!=root_link_name_) {
          single_branch.push_back(linkb.name);
          linkb = *urdf_model_.links_[linkb.getParent()->name];
      }
      single_branch.push_back(root_link_name_);
      branches_.push_back(single_branch);
  }

  //ToDO: check that there are no closed-loop branches
  branhces_ready_ = true;

  if(debug_mode_) {
      size_t b_number = 0;
      std::cout << "The following branchs have been found:" << std::endl;
      for(auto b:branches_){
          std::cout << "branch " << b_number << " : "  << std::endl;
        for(auto l : b){
            std::cout << l << std::endl;
        }
        std::cout << "------" << std::endl;
        b_number++;
      }
      std::cout << "press <any key> to continue..." << std::endl;
      std::getchar();
  }
}

bool Urdf2RobCoGen::generateFiles() {

  //generate_branches();
  bool f1 = generateKindsl();
  bool f2 = generateDtDsl();
  return (f1 && f2);

}

bool Urdf2RobCoGen::generateKindsl() {

  if(!frames_names_ready_ || !link_names_ready_) {
      ROS_ERROR("Links and Frames names should be set before generating the Kindsl file");
      std::exit(EXIT_FAILURE);
      return false;
  }
  compute_branches();
  // fix frames!
  fix_URDF_frames();


  std::string kindsl_name = robot_name_ + ".kindsl";
  size_t id=1;

  std::ofstream kindsl_file(kindsl_name.c_str());

  kindsl_file << "Robot " << robot_name_ <<  "{" << std::endl;
  kindsl_file << "RobotBase " << root_link_name_ << " floating " << "{" << std::endl;
  //ToDo: check if the base has inertia params otherwise search for fake base
  kindsl_file << print_inertia_params(*urdf_model_.root_link_);
  kindsl_file << print_children(*urdf_model_.root_link_);
  kindsl_file << print_frame(*urdf_model_.root_link_);
  kindsl_file << "}" << std::endl;

  // links
  for(auto l_name : robot_link_names_) {
    if(l_name != root_link_name_) {
      kindsl_file << std::endl;
      kindsl_file << "link " << l_name << " {" << std::endl;
      kindsl_file << "\t id = " << id << std::endl;
      kindsl_file << print_inertia_params(*urdf_model_.links_[l_name]);
      kindsl_file << print_children(*urdf_model_.links_[l_name]);
      kindsl_file << print_frame(*urdf_model_.links_[l_name]);
      id++;
      kindsl_file << "}" << std::endl;
    }
  }

  // joints (ToDo: get type from model)
  for(auto j_name : robot_joint_names_) {
      kindsl_file << std::endl;
      kindsl_file << "r_joint " << j_name << " {" << std::endl;
      kindsl_file << "\t ref_frame {" << std::endl;
      kindsl_file << "\t\t " << print_translation(*urdf_model_.joints_[j_name]);
      kindsl_file << "\t\t " << print_rotation(*urdf_model_.joints_[j_name]);
      kindsl_file << "\t }" << std::endl;
      kindsl_file << "}" << std::endl;
  }

  //end of robot
  kindsl_file << "}" << std::endl;

  kindsl_file.close();

  //ToDo: check if the file closed correctly
  return(true);
}

bool Urdf2RobCoGen::generateDtDsl() {

  if(!ee_names_ready_ || !link_names_ready_ || !frames_names_ready_) {
      ROS_ERROR("Links, frames and EE names should be set before generating the DtDsl file");
      std::exit(EXIT_FAILURE);
      return false;
  }

  std::string dtdsl_name  = robot_name_ + ".dtdsl";
  std::ofstream dtdsl_file(dtdsl_name.c_str());

  dtdsl_file << "Robot " << robot_name_ << std::endl;
  dtdsl_file << "Frames {" << std::endl;
  dtdsl_file << list_frames()<< std::endl;
  dtdsl_file << "}" <<std::endl;

  dtdsl_file << "Transforms {" << std::endl;
  dtdsl_file << list_transforms() << std::endl;
  dtdsl_file << "}" << std::endl;

  dtdsl_file << "Jacobians {" << std::endl;
  dtdsl_file << list_jacobians() << std::endl;
  dtdsl_file << "}" << std::endl;

  dtdsl_file.close();

  //ToDo: check if the file closed correctly
  return(true);
}

std::string Urdf2RobCoGen::list_frames() const {

  std::ostringstream out;
  int k = 0;

  // the root link frame
  out << "\t " << "fr_" << root_link_name_ << ", " << std::endl;

  //extra frames
  out << "\t ";
  for(auto ee : ee_names_) {
      out << "fr_" << ee << ", ";
  }
  out << std::endl;

  // All frames
  out << "\t ";
  for(auto link_name : robot_link_names_) {
      if(k > 3 ) {
          out << std::endl;
          out << "\t ";
          k = 0;
      }
      out << "fr_" << link_name;
      if(link_name != robot_link_names_.back()) {
          out << ", ";
      }
      k++;
  }
  return out.str();
}

std::string Urdf2RobCoGen::list_transforms() {

  std::ostringstream out;
  typedef std::pair<std::string,std::string> transform_pair;
  transform_pair mpair;
  std::vector<std::pair<std::string,std::string>> all_pairs;

  for(auto single_branch:branches_) {
      for(auto base_name:single_branch) {
          //transforms among all the elements of the branch
          for(auto target_name:single_branch) {
              if(target_name == base_name)
                continue;
              mpair=transform_pair(base_name,target_name);
              bool good = true;
              for(auto test:all_pairs) {
                  if(mpair == test)
                      good = false;
              }
              if(good) {
                  all_pairs.push_back(mpair);
                  out << "\t base=fr_" << mpair.first << ", target=fr_" << mpair.second << std::endl;
              }
          }
      }
  }
  return out.str();
}

std::string Urdf2RobCoGen::list_jacobians() {

  //Note: Don't request jacobians from frames of the same link!

  std::ostringstream out;
  for(auto ee_name: ee_names_) {
      if(frames_[ee_name].parent_link_name != root_link_name_) {
          out << "\t base=fr_" << root_link_name_ << ", target=fr_" << ee_name;
          if(ee_name!=ee_names_.back()) {
              out << std::endl;
          }
      }
  }
  return out.str();
}

std::string Urdf2RobCoGen::print_inertia_params(const urdf::Link & link) const {

  std::ostringstream out,mass,xx,yy,zz,xy,xz,yz;
  int digits = 10;
  out.precision(10);
  mass.precision(10);
  xx.precision(10);
  yy.precision(10);
  zz.precision(10);
  xy.precision(10);
  yz.precision(10);
  xz.precision(10);

  out << "\t inertia_params {" << std::endl;

  if(!link.inertial) {
      ROS_WARN("MISSING INERTIA PARAMTERES LINK:");
      std::cout << link.name << std::endl;
      out << "\t }" << std::endl;
      return out.str();
  }

  mass << std::fixed << link.inertial->mass;
  xx << std::fixed << link.inertial->ixx;
  yy << std::fixed << link.inertial->iyy;
  zz << std::fixed << link.inertial->izz;
  xy << std::fixed << link.inertial->ixy;
  xz << std::fixed << link.inertial->ixz;
  yz << std::fixed << link.inertial->iyz;

  out <<
  "\t\t mass = "<< mass.str()<< std::endl <<
  "\t\t CoM = " << print_vector(link.inertial->origin.position) << '\n' <<
  "\t\t Ix = "  << std::fixed << xx.str()  << std::endl <<
  "\t\t Iy = "  << std::fixed << yy.str()  << std::endl <<
  "\t\t Iz = "  << std::fixed << zz.str()  << std::endl <<
  "\t\t Ixy = " << std::fixed << xy.str()  << std::endl <<
  "\t\t Ixz = " << std::fixed << xz.str()  << std::endl <<
  "\t\t Iyz = " << std::fixed << yz.str()  << std::endl <<
  //"\t\t ref_frame = " << "fr_" << link.name << "_com" << std::endl <<
  "\t }" << std::endl;

  return out.str();
}

std::string Urdf2RobCoGen::print_children(const urdf::Link & link) {

  std::ostringstream out;
  bool style = false;
  out << "\t " << "children " << "{";
  for(std::string link_name : robot_link_names_ ) {
      if(link_name != root_link_name_) {
        auto test_link = urdf_model_.links_[link_name];
        //std::cout << "printing childrens of: " << link_name << std::endl;
        if(test_link->getParent()->name == link.name) {
            if(!style) {
                out << std::endl;
                style = true;
            }
            out << "\t" << "\t " << test_link->name << " via " << test_link->parent_joint->name <<  std::endl;
        }
      }
  }
  if(style)
      out << "\t ";
  out << "}" << std::endl;
  return out.str();
}

std::string Urdf2RobCoGen::print_translation(const urdf::Joint & joint) {

  std::ostringstream out;

  // the position of the joint expressed in the parent_link frame
  out << "translation = " << print_vector(joint.parent_to_joint_origin_transform.position)  << std::endl;
  return out.str();
}

std::string Urdf2RobCoGen::print_rotation(const urdf::Joint & joint) {

  std::ostringstream out;

  // the orientation of the joint expressed in the parent_link frame
  out << "rotation = " << print_vector(rotation_vector(joint.parent_to_joint_origin_transform.rotation)) << std::endl;

  return out.str();
}

urdf::Vector3 Urdf2RobCoGen::rotation_vector(const urdf::Rotation & rot_quaternion) {

  urdf::Vector3 rot_vec;
  Eigen::Matrix<double,3,1> p_min;
  kindr::RotationQuaternionD quat(rot_quaternion.w,rot_quaternion.x,rot_quaternion.y,rot_quaternion.z);

  kindr::EulerAnglesXyz<double> temp(quat);
  kindr::EulerAnglesXyz<double> xyz_unique = temp.getUnique();
  double x,y,z;
  double epsilon = 1e-4;
  std::fabs(xyz_unique.yaw()) < epsilon ? z = 0.0 : z = xyz_unique.yaw();
  std::fabs(xyz_unique.pitch()) < epsilon ? y = 0.0 : y = xyz_unique.pitch();
  std::fabs(xyz_unique.roll()) < epsilon ? x = 0.0 : x = xyz_unique.roll();

  rot_vec = urdf::Vector3(x,y,z);

  return rot_vec;
}

std::string Urdf2RobCoGen::print_frame(const urdf::Link & link) {

  std::ostringstream out;
  bool print_title = false;
  for(std::pair<std::string,urdf::Joint> f:frames_) {
      if(f.second.parent_link_name == link.name) {
          if(!print_title) {
              out << "\t frames {" << std::endl ;
              print_title = true;
          }
          out << "\t \t fr_" << f.second.name << " { " << std::endl;
          out << "\t \t \t " << print_translation(f.second);
          out << "\t \t \t " << print_rotation(f.second);
          out << "\t \t }" << std::endl;
      }
  }
  if(print_title) {
      out << "\t }" << std::endl;
  }

  return out.str();
}

std::string Urdf2RobCoGen::print_vector(const urdf::Vector3 & vector) const {

  std::ostringstream out,x,y,z;
  out.precision(17);

  x << std::fixed << vector.x;
  y << std::fixed << vector.y;
  z << std::fixed << vector.z;
  out << "("<< x.str() << "," << y.str() << "," << z.str() << ")";
  return out.str();
}

void Urdf2RobCoGen::fix_URDF_frames() {

  // RobCogen assumes that the rotation axis is always Z.
  // The rotation axis of a joint in an URDF is specified by the variable joint.axis
  // the transformation from parent_link to joint should change :

  // We modify the original properties of the urdf before
  // parsing and creating the RobCoGen files

  // We check the rigid body chain in order as the joint frame
  // affects the link frame and therefore the transformation
  // to the next joint

  // I. first pass: new transformation of each link (joint father)

  QuaternionMap_t Tn;  /* Transformation to align the Z axis with the Joint */
  QuaternionMap_t To;  /* Original transformation from parent link to joint */
  for(auto j: robot_joint_names_) {
      urdf::Joint test = *urdf_model_.joints_[j];

      kindr::RotationQuaternionD Tn_i, To_i;
      Eigen::Vector3d axis;
      std::string about;
      double angle;

      // The original transformation of the joint from parent link
      // notice that the parser assumes the URDF uses "RPY on a fixed axis" which is equivalent to "YPR on a rotation axis"
      // however, RobCoGen uses "RPY on a rotation axis".
      To_i = kindr::RotationQuaternion<double>(test.parent_to_joint_origin_transform.rotation.w,
                                test.parent_to_joint_origin_transform.rotation.x,
                                test.parent_to_joint_origin_transform.rotation.y,
                                test.parent_to_joint_origin_transform.rotation.z);

      //check the rotation axis
      if(test.axis.x == 1) {
          axis = Eigen::Vector3d::UnitY();
          angle = M_PI/2;
          about = "Y";
      } else {
          if(test.axis.y == 1) {
              axis = Eigen::Vector3d::UnitX();
              angle = -M_PI/2;
              about = "X";
          }
          else{ // rotation is in z
              axis = Eigen::Vector3d::UnitZ();
              angle = 0.0;
              about = "Z";
          }
      }
      Tn_i = kindr::RotationQuaternionD(kindr::AngleAxis<double>(angle, axis));
      Tn[j]= Tn_i;
      To[j]= To_i;

    if(debug_mode_){
     std::cout << j << " : " << std::endl;
     std::cout << "Rotating about : " << about << std::endl;
     kindr::EulerAnglesXyzD eang_Tn(Tn_i);
     kindr::EulerAnglesXyzD eang_To(To_i.inverted());
     std::cout << "Tn euler : " << eang_Tn.getUnique().vector().transpose() << std::endl;
     std::cout << "To euler : " << eang_To.getUnique().vector().transpose() << std::endl;
     std::getchar();
    }
  }

  // II. Second pass: find the new 'parent' transformations only for branches!
  // Assumption : The user wants to use a inertial frame with the z axis aligned with gravity.

  // Note: Transformations are fixed following branches
  for(auto single_branch : branches_) {
      kindr::RotationQuaternionD Tn_1;
      for(int k = single_branch.size()-1 ; k > 0; --k) {
          std::string link = single_branch[k];
          std::cout << "LINK : " << link << std::endl;
          if(link !=root_link_name_) {
           kindr::RotationQuaternionD T_bar_i;
           std::string joint = urdf_model_.links_[link]->parent_joint->name;

           T_bar_i = Tn_1.inverted()*  To[joint] * Tn[joint];
           urdf_model_.joints_[joint]->parent_to_joint_origin_transform.rotation.setFromQuaternion(T_bar_i.x(),T_bar_i.y(),T_bar_i.z(),T_bar_i.w());

           if(debug_mode_) {
               kindr::EulerAnglesXyzD eang_Tbar(T_bar_i);
               kindr::EulerAnglesXyzD eang_Tn1inv(Tn_1.inverted());
               kindr::EulerAnglesXyzD eang_To(To[joint]);
               kindr::EulerAnglesXyzD eang_Tn(Tn[joint]);

               std::cout << "joint : " << joint << std::endl;
               std::cout << "T_n_1_invert : " << eang_Tn1inv.getUnique().vector().transpose() << std::endl;
               std::cout << "T_o : " << eang_To.getUnique().vector().transpose() << std::endl;
               std::cout << "T_n : " << eang_Tn.getUnique().vector().transpose() << std::endl;
               std::cout << "T_bar :" << eang_Tbar.getUnique().vector().transpose() << std::endl;
               std::getchar();
           }

           // fix the position of the joint p' = R'p
           urdf::Vector3 p = urdf_model_.joints_[joint]->parent_to_joint_origin_transform.position;
           Eigen::Vector3d p_o; p_o << p.x, p.y, p.z;
           Eigen::Vector3d p_n = Tn_1.toImplementation().toRotationMatrix().transpose()*p_o;
           p = urdf::Vector3(p_n(0), p_n(1), p_n(2));
           urdf_model_.joints_[joint]->parent_to_joint_origin_transform.position = p;

           if(debug_mode_){
               std::cout << std::endl;
               std::cout << "joint: " << joint << std::endl;
               std::cout << "Po : " <<  p_o.transpose() << std::endl;
               kindr::EulerAnglesXyzD eang_Tn(Tn_1);
               std::cout << "R_angle : " << eang_Tn.getUnique().vector().transpose() << std::endl;
               std::cout << "R : " << std::endl << Tn_1.toImplementation().toRotationMatrix() << std::endl;
               std::cout << "P1 :" << p_n.transpose() << std::endl;
               std::getchar();
           }

           // fix orientation of the CoM frame
           std::string com_frame = link + "_com";
           urdf::Rotation q = frames_[com_frame].parent_to_joint_origin_transform.rotation;
           kindr::RotationQuaternionD T_com(q.w,q.x,q.y,q.z);
           kindr::RotationQuaternionD T_com_n = Tn[joint].inverted()*T_com;
           frames_[com_frame].parent_to_joint_origin_transform.rotation.setFromQuaternion(T_com_n.x(),T_com_n.y(),T_com_n.z(),T_com_n.w());

           // Fix position of the CoM frame
           urdf::Vector3 com = frames_[com_frame].parent_to_joint_origin_transform.position;

           // original origin of the CoM
           Eigen::Vector3d com_o;
           com_o << com.x, com.y , com.z;
           std::cout << "com_o : " << com_o.transpose() << std::endl;
           std::getchar();

           // new origin of the CoM (p' = R'p)
           Eigen::Vector3d com_n = Tn[joint].toImplementation().toRotationMatrix().transpose()*com_o;
           com = urdf::Vector3(com_n(0),com_n(1),com_n(2));
           frames_[com_frame].parent_to_joint_origin_transform.position = com;
           urdf_model_.links_[link]->inertial->origin.position = com;

           if(debug_mode_){
               kindr::EulerAnglesXyzD tcom(T_com);
               std::cout << "link_To_com : " << tcom.getUnique().vector().transpose() << std::endl;
               std::cout << "CoM_o : " << com_o.transpose() << std::endl;
               std::cout << "CoM_n : " << com_n.transpose() << std::endl;
               std::getchar();
           }

           // Fix the Inertia parameters (here?)
           // 1- Translation to the original Link origin
           // I(L) = I(G) + M(R^2*d_ij - RiRj)

           Eigen::Matrix<double,3,3> I_C, I_Lo, I_Ln;    /* Inertia tensor expressed in the NEW link frame */
           double R_n = com_o.squaredNorm(); /* distance to the original link origin */

           // 1-a : change sign of off-diagonal elements to get the inertia tensor!
           I_C(0,0) = urdf_model_.links_[link]->inertial->ixx;
           I_C(0,1) = urdf_model_.links_[link]->inertial->ixy;
           I_C(0,2) = urdf_model_.links_[link]->inertial->ixz;
           I_C(1,0) = I_C(0,1);
           I_C(1,1) = urdf_model_.links_[link]->inertial->iyy;
           I_C(1,2) = urdf_model_.links_[link]->inertial->iyz;
           I_C(2,0) = I_C(0,2);
           I_C(2,1) = I_C(1,2);
           I_C(2,2) = urdf_model_.links_[link]->inertial->izz;

           double mass = urdf_model_.links_[link]->inertial->mass;

           std::cout << "I_C : " << std::endl;
           std::cout << I_C << std::endl;
           std::getchar();

           // 1-b : get the new Inertia matrix (translated to the origin of the OLD link frame)

           I_Lo(0,0) = I_C(0,0) + mass*(R_n - com_o(0)*com_o(0));
           I_Lo(0,1) = I_C(0,1) + mass*(-com_o(0)*com_o(1));
           I_Lo(0,2) = I_C(0,2) + mass*(-com_o(0)*com_o(2));
           I_Lo(1,0) = I_C(1,0) + mass*(-com_o(1)*com_o(0));
           I_Lo(1,1) = I_C(1,1) + mass*(R_n - com_o(1)*com_o(1));
           I_Lo(1,2) = I_C(1,2) + mass*(-com_o(1)*com_o(2));
           I_Lo(2,0) = I_C(2,0) + mass*(-com_o(2)*com_o(0));
           I_Lo(2,1) = I_C(2,1) + mass*(-com_o(2)*com_o(1));
           I_Lo(2,2) = I_C(2,2) + mass*(R_n - com_o(2)*com_o(2));

           std::cout << "I_Lo (before rotation): " << std::endl;
           std::cout << I_Lo << std::endl;
           std::getchar();

           // 2: Rotate the tensor

           // Rotation Matrix describing the new Link frame (p = Rp')
           Eigen::Matrix3d o_R_n = Tn[joint].toImplementation().toRotationMatrix();
           Eigen::Matrix3d n_R_o = o_R_n.transpose();

           // The new Inertia tensor;
           I_Ln = n_R_o*I_C*o_R_n;

           // 2-b fill back the inertia parameters, now changing the sign of off diagonal
           urdf_model_.links_[link]->inertial->ixx = I_Ln(0,0);
           urdf_model_.links_[link]->inertial->iyy = I_Ln(1,1);
           urdf_model_.links_[link]->inertial->izz = I_Ln(2,2);
           urdf_model_.links_[link]->inertial->ixy = I_Ln(0,1);
           urdf_model_.links_[link]->inertial->ixz = I_Ln(0,2);
           urdf_model_.links_[link]->inertial->iyz = I_Ln(1,2);

           // test:
           std::cout << "I_Ln : " << std::endl;
           std::cout << I_Ln << std::endl;
           std::getchar();

           //and Tada!!.. your new inertia parameters have been roto-translated (in the correct way!)
           Eigen::LLT<Eigen::MatrixXd> lltOfA(I_Ln); // compute the Cholesky decomposition of A
           if(lltOfA.info() == Eigen::NumericalIssue)
           {
              throw std::runtime_error("Possibly non semi-positive definitie matrix!");
           }
           else {
               Eigen::VectorXcd eiv = I_Ln.eigenvalues();
               std::cout << "The Matrix is ok!" << std::endl;
               std::cout << "Eigenvalues : " << eiv.transpose() << std::endl;
           }
           std::getchar();
           // save the value of the current transformation for fixing the next one
           Tn_1 = Tn[joint];
          }
      }
  }
  // Fixed!
}

void Urdf2RobCoGen::showLinkJointNames() {

  std::cout << "Link Names: " << std::endl;
  for(auto l_name : robot_link_names_) {
    std::cout << l_name << std::endl;
  }
  std::cout << "press <any key> to continue..." << std::endl;
  std::getchar();

  std::cout << "Joint Names: " << std::endl;
  for(auto j_name : robot_joint_names_) {
    std::cout << j_name << std::endl;
  }

  std::cout << "Orphan Frames: " << std::endl;
  for(auto o_links : orphan_frames_) {
      std::cout << o_links << std::endl;
  }

  std::cout << "Orphan Joints: " << std::endl;
  for(auto o_joints : orphan_joints_) {
      std::cout << o_joints.first << std::endl;
  }

  std::cout << "press <any key> to continue..." << std::endl;
  std::getchar();

}

#endif /* _URDF2ROBCOGEN_HPP_ */
//#include <dynamical_systems/systems/anymal/iit/miscellaneous.h>
